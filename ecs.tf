# Configure the AWS provider
provider "aws" {
  region = "us-west-2"
}

# Create an ECS cluster
resource "aws_ecs_cluster" "ecs_cluster" {
  name = "my-ecs-cluster"
}

# Create a task definition
resource "aws_ecs_task_definition" "ecs_task_definition" {
  family                   = "my-task-definition"
  container_definitions    = <<EOF
[
  {
    "name": "my-container",
    "image": "nginx:latest",
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80,
        "protocol": "tcp"
      }
    ]
  }
]
EOF
}

# Create a service to run tasks on the ECS cluster
resource "aws_ecs_service" "ecs_service" {
  name            = "my-ecs-service"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.ecs_task_definition.arn
  desired_count   = 2

  deployment_controller {
    type = "CODE_DEPLOY"
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.lb_target_group.arn
    container_name   = "my-container"
    container_port   = 80
  }
}

# Create a load balancer and target group
resource "aws_lb" "lb" {
  name               = "my-load-balancer"
  load_balancer_type = "application"
  subnets            = ["subnet-12345678", "subnet-98765432"]
}

resource "aws_lb_target_group" "lb_target_group" {
  name     = "my-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "vpc-12345678"
}

resource "aws_lb_listener" "lb_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.lb_target_group.arn
    type             = "forward"
  }
}

# Output the ECS cluster and service details
output "ecs_cluster_arn" {
  value = aws_ecs_cluster.ecs_cluster.arn
}

output "ecs_service_arn" {
  value = aws_ecs_service.ecs_service.arn
}
